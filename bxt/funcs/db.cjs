const fs = require('fs'); // Use require instead of import for fs
const mysql = require('mysql2/promise'); // Use require for mysql2
const path = require('path'); // Use require for path

// Function to execute SQL queries
async function executeQuery(query, params = []) {
		// Create the connection pool. The pool-specific settings are the defaults
		const pool = mysql.createPool({
			host: 'bxt-mariadb-1',
			user: 'someuser',
			password: 'somepass',
			database: 'bxt',
			waitForConnections: true,
			connectionLimit: 10,
			maxIdle: 10, // max idle connections, the default value is the same as `connectionLimit`
			idleTimeout: 60000, // idle connections timeout, in milliseconds, the default value 60000
			queueLimit: 0,
			enableKeepAlive: true,
			keepAliveInitialDelay: 0,
		});

    	// Wait until the DB is available
    	let isConnected = false;
    	while (!isConnected) {
    	    isConnected = await attemptConnect(pool);
    	    if (!isConnected) {
    	        console.log('Retrying connection in 5 seconds...');
    	        await new Promise(resolve => setTimeout(resolve, 5000)); // Wait 5 seconds before retrying
    	    }
    	}

		const clean_query = cleanSql(query);

		return new Promise((resolve, reject) => {
				pool.query(clean_query, params, (err, results) => {
						if (err) {
								reject(err);
						} else {
								resolve(results);
						}
				});
		});
}

// Function to attempt database connection
async function attemptConnect(pool) {
    try {
        // Attempt to acquire a connection to check if the DB is available
        const connection = await pool.getConnection();
        connection.release(); // Release the connection back to the pool
        return true; // Connection successful
    } catch (error) {
        console.error('Error connecting to the database:', error.message);
        return false; // Connection failed
    }
}

// Cleans SQL in a variable
function cleanSql(query) {
	// Read the file synchronously in 'utf8' encoding
	let sqlQuery = query;

	// Remove all newlines (\n) and carriage returns (\r) by replacing them with a space
	sqlQuery = sqlQuery.replace(/[\r\n]+/g, ' ');

	// Remove all tabs (\t) and replace them with a space
	sqlQuery = sqlQuery.replace(/\t+/g, ' ');

	// Optional: Remove multiple spaces and replace them with a single space
	sqlQuery = sqlQuery.replace(/\s+/g, ' ').trim();

	// Optional: Remove any leading or trailing spaces around semicolons or SQL statements
	sqlQuery = sqlQuery.replace(/\s*([;,])\s*/g, '$1');	// Tighten spaces around semicolons and commas
	console.log(sqlQuery);

	return sqlQuery;
}

// Attempts to clean a sql file for execution
function cleanSqlFile(filePath) {
	// Read the file synchronously in 'utf8' encoding
	let sqlQuery = fs.readFileSync(filePath, 'utf8');

	// Use above function to clean instead
	sqlQuery = cleanSql(sqlQuery);
	
	// Return sanitized results
	return sqlQuery;
}

// Initialize the database (create tables, etc.)
async function initializeDb() {

	try {
		// Read all files from the directory
		const directoryPath = 'install/sql';
		const files = fs.readdirSync(directoryPath);

		// Filter to get only .sql files
		const sqlFiles = files.filter(file => file.endsWith('.sql'));

		// Sort the files to ensure they are executed in the correct numeric order
		sqlFiles.sort((a, b) => {
			return a.localeCompare(b, 'en', { numeric: true });
		});

		// Execute each SQL file in sorted order
		for (const file of sqlFiles) {
			var filePath = path.join(directoryPath, file);
			var sqlQuery = cleanSqlFile(filePath);
			const queries = sqlQuery.split(';').filter(query => query.trim() !== '');
			console.log(queries);
			for (let query of queries) {
				try {
					await executeQuery(query); // Execute the current query
					console.log(`Query executed successfully from file: ${filePath}`);
				} catch (err) {
					console.error(`Error executing query from file ${filePath}:`, err);
				}
			}
		}
	} catch (err) {
		console.error('Error reading files from directory:', err);
	} finally {
		await pool.end(); // Close the connection pool
	}
}

module.exports = {
		executeQuery,
		initializeDb
};

