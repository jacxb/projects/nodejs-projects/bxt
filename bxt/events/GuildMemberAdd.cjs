const { Events } = require('discord.js');

module.exports = {
	name: Events.GuildMemberAdd,
	async execute(GuildMember) {
		const guild_joined = GuildMember.guild;
		const guild_members = await guild_joined.members.fetch();

		if ( guild_joined.members == 1 && guild_members.first().user.bot )
		{
			const first_admin_role = await guild_joined.roles.create({
				name: "Admin",
				reason: "Original admin account",
				permissions: [Permissions.FLAGS.ADMINISTRATOR],
			})

			await GuildMember.roles.add(first_admin_role)
		}
	},
};
