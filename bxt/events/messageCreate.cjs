const { Events } = require('discord.js');
const { executeQuery } = require('../funcs/db.cjs');

module.exports = {
	name: Events.MessageCreate,
	async execute(message) {
		const guildId		= message.guild.id;
		const channelId		= message.channel.id;
		const messageId		= message.id;
		const authorId		= message.author.id;
		const messageContent = message.content;
		const messageDate	= message.createdAt.toISOString().slice(0, 19).replace('T', ' ');

		// Use a parameterized query to prevent SQL injection
		const query = `
			INSERT INTO messages (guild_id, channel_id, message_id, author_id, content, date_created) 
			VALUES (?, ?, ?, ?, ?, ?);
		`;
		const values = [guildId, channelId, messageId, authorId, messageContent, messageDate];

		// Execute the query using parameterized query method
		try {
			// Execute the query
			await executeQuery(query, values);
			console.log(`Message successfully inserted: ${messageId}`);
		} catch (error) {
			// If there's an error, log it
			console.error(`Error inserting message ${messageId}:`, error);
		}
	},
};
