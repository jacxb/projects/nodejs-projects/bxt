const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('register')
		.setDescription('Register your account to be whitelisted on our game servers'),
	async execute(interaction) {
		await interaction.reply('Pong!');
	},
};
