const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('guild_delete')
		.setDescription('Delete the specified GuildID (if bot has permission)')
		.addStringOption( option =>
			option
				.setName("guild_id")
				.setDescription("The ID of the guild that you want to delete. Bot must be owner of this guild.")
				.setRequired(true)
		),
	async execute(interaction) {
		var bot_client_id	= interaction.client.user.id
		var guild_id 			= interaction.options.getString("guild_id")
		var requestor_id	= interaction.user.id

		try {
			var guild = interaction.client.guilds.cache.get(guild_id)
		} catch (error) {
				console.log(error)
		}

		if ( typeof(guild) != undefined ) {
			guild.fetchOwner().then( owner => {
				if ( owner.id == bot_client_id ) {
					if ( requestor_id == "214577944570494976" ) {
						guild.delete()
						  .then(guild => {
								console.log(`Deleted the guild ${guild}`)
								interaction.reply(`Guild ${guild} has been deleted.`)
							})
  						.catch(console.error);
					}
				} else {
					interaction.reply("I can't delete this guild because I don't own it.")
				}
			})
		} else {
			interaction.reply(`Could not find guild ${guild_id}`)
		}
	}
};
