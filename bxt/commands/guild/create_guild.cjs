const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('guild_create')
		.setDescription('Create a guild with the bot as the owner')
		.addStringOption( option =>
			option
				.setName("guild_name")
				.setDescription("The name of the guild that you want to create")
				.setRequired(true)
		),
	async execute(interaction) {
		var guild_name = interaction.options.getString("guild_name")
		var new_guild = interaction.client.guilds.create({ name: guild_name }).then(guild => {
			guild.channels.create({
				name: 'invite-channel',
				reason: 'easiest way to invite you. please delete'
			})
				.then( channel => {
					channel.createInvite()
						.then(invite => interaction.reply({ content: `Created an invite with a code of ${invite.url}`, ephemeral: true }))
				})
		})
	},
};
