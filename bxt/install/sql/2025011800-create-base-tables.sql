CREATE TABLE IF NOT EXISTS `messages` (
	`message_id` bigint NOT NULL UNIQUE,
	`guild_id` bigint NOT NULL,
	`channel_id` bigint NOT NULL,
	`author_id` bigint NOT NULL,
	`content` text NOT NULL,
	`date_created` DATETIME NOT NULL,
	PRIMARY KEY (`message_id`)
);

CREATE TABLE IF NOT EXISTS `message_reactions` (
	`message_id` int NOT NULL,
	`reactor_id` bigint NOT NULL,
	`reaction` text NOT NULL
);

CREATE TABLE IF NOT EXISTS `members` (
	`member_id` bigint NOT NULL UNIQUE,
	`name` text NOT NULL,
	PRIMARY KEY (`member_id`)
);

CREATE TABLE IF NOT EXISTS `guilds` (
	`guild_id` bigint NOT NULL UNIQUE,
	PRIMARY KEY (`guild_id`)
);

CREATE TABLE IF NOT EXISTS `channels` (
	`channel_id` bigint NOT NULL UNIQUE,
	`guild_id` bigint NOT NULL,
	PRIMARY KEY (`channel_id`)
);

CREATE TABLE IF NOT EXISTS `auto_voice_channels_monitored` (
	`channel_id` bigint NOT NULL UNIQUE,
	PRIMARY KEY (`channel_id`)
);

CREATE TABLE IF NOT EXISTS `auto_voice_channels_created` (
	`channel_id` bigint NOT NULL UNIQUE,
	`creator_id` int NOT NULL,
	PRIMARY KEY (`channel_id`)
);

CREATE TABLE IF NOT EXISTS `member_guilds` (
	`member_id` bigint NOT NULL,
	`guild_id` bigint NOT NULL
);
