# BXT

A basic Discord bot written in NodeJS.

**Due to limitations with Discords API, this bot only supports self-hosting capabilities. This bot will not be available to be invited into your Discord**

*I might change my mind on this one day and release a public-friendly version of this bot, however I'm just coding it to have the functionality that I want.*

## Planned Features

1. [ ] Dynamic Voice Channels
    1. [ ] Create voice channel when someone joins a lobby
    2. [ ] Give commands for user to:
        1. [ ] Toggle private channel
        2. [ ] Invite someone to private channel
        3. [ ] Change voice channels name
    3. [ ] Detect joining users region and create the VC there
    4. [ ] Detect Guild boost level and apply max quality
2. [ ] Game Server Whitelisting
    1. [ ] Configure method of correlating a Discord user to an in-game ID (such as Steam)
    2. [ ] Apply users ID to some kind of white list 
    3. [ ] Do the above for the below games:
        1. [ ] Minecraft
        2. [ ] Ark (ASE and ASA)
    4. [ ] Ban discord member blacklists their in-game ID
        1. [ ] Add reason to database
3. [ ] Recreate mortgage calculator because that was cool
4. [ ] Music Bot 
    1. [ ] Join multiple channels concurrently
    2. [ ] Services:
        1. [ ] Spotify
        2. [ ] YouTube
5. [ ] Message logging
    1. [ ] Log to database
    2. [ ] Print message history around quoted message
6. [ ] Invite Tracker
    1. [ ] Assign role based on invite 
    2. [ ] Generate invite and assign to role
7. [ ] Ticketing
    1. [ ] Create a channel for a ticket with certain role + member
    2. [ ] Ticket Statuses & Closure (How to close??)
8. [ ] Moderation
    1. [ ] Kick with reason, reason logged to DB and audit log
    2. [ ] Ban with reason, reason logged to DB and audit log
9. [ ] Server Stats
    1. [ ] Noticed that [https://top.gg/bot/1248237632148733954](MultiTool) does this. Should emulate.
10. [ ] Fun
    1. [ ] Fly facts
    2. [ ] Get annoyed when someone says 'cunt'
    3. [ ] Memes? dad jokes, general jokes, whatever, pull from a list somewhere
    4. [ ] React to screenshot? Or quote with command to screenshot?
11. [ ] User automation
    1. Some of these are repeats 
    2. [ ] Log all messages
    3. [ ] Log user <-> Discord Guild
    4. [ ] Apply role based on activity
    5. [ ] Auto role bot maybe, but Discord has that built in now...
    6. [ ] Change nick to have an emoji at the start or end? Like a badge, but hacky... 
    7. [ ] 
12. [ ] Go-Live notifier? 
    1. [ ] Watch twitch channel, see when online. If online, tag a role in a channel
    2. [ ] As above but with YouTube
13. [ ] RCON
    1. [ ] Permission based on user role
    2. [ ] Runs command on server, puts output into Discord
14. [ ] Extras / API
    1. [ ] API to send something to bot and have it do something in Discord
        1. [ ] API could allow configuration on a per-guild basis
    2. [ ] Create guild, so bot owns guild.
    3. [ ] Can NodeJS handle binary files? I wonder if I could do something with Ark files to get tribe logs and notify.
    4. [ ] ChatGPT integration would be cool. Double points if we can listen to voice commands now.
    5. [ ] Emoji & avatar stealing? https://top.gg/bot/1249902362357796864
    6. [ ] Embed for join/leave?? https://top.gg/bot/873934253468024852
    7. [ ] Would be silly not to implement everything that YAGDB does: https://top.gg/bot/204255221017214977
    8. [ ] IRC integration would be kinda fun/funny to add... Could do embeds for messages or something.
    9. [ ] Doing commands by mentioning the bot would be cool, with ChatGPT responses, if I can integrate that.
        1. [ ] This one looks really good. Has image generation, which would be fun. https://github.com/Zero6992/chatGPT-discord-bot
        2. [ ] Maybe restrict ChatGPT with image generation ... need to check api costs.
        3. [ ] If API costs money then bot should DM someone on command and confirm that they know it costs money. If they accept, it wont message again. Onus is on Guild admin to tell people who have role to create images.
    10. [ ] Listening is apparently still possible; worth exploring: https://www.reddit.com/r/Discordjs/comments/xekas9/discord_js_14_receiving_audio_from_voice_channels/
        1. [ ] Could do Google Assistant style thing.
        2. [ ] Could do call bridging trick maybe?
15. [ ] Games
    1. [ ] WoW
    2. [ ] Ark SE
        1. [ ] Pull logs, look for kills, do something with kill stats
    3. [ ] Ark SA
    4. [ ] CS2 Skins??? 
    5. [ ] LoL & DOTA stats? 
