/* Chat Log */
CREATE TABLE channel_chat_log (
	id int NOT NULL AUTO_INCREMENT,
	guild_id int,
	channel_id int,
	author_id int,
	content text,
	primary key(id)
);

CREATE TABLE dm_chat_log (
	id int NOT NULL AUTO_INCREMENT,
	bot_id int,
	member_id int,
	content text,
	primary key(id)
);

/* GSA Stuff */
CREATE TABLE gsa_servers (
	id int NOT NULL AUTO_INCREMENT,
	name text,
	address text,
	primary key(id)
);

CREATE TABLE gsa_whitelists (
	id int NOT NULL AUTO_INCREMENT,
	name text,
);

CREATE TABLE gsa_accounts (
	id int NOT NULL AUTO_INCREMENT,
	discord_id int,
	ingame_id text,
	banned boolean default false,
	in_discord boolean default false,
	primary key(id)
);

/* Roles */
CREATE TABLE roles (
	id int NOT NULL AUTO_INCREMENT,
	guild_id int,
	name text,
	color text,
	display_separately boolean,
	allow_mention boolean,
	primary key(id)
);
